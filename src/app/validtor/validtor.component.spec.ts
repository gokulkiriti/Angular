import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidtorComponent } from './validtor.component';

describe('ValidtorComponent', () => {
  let component: ValidtorComponent;
  let fixture: ComponentFixture<ValidtorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidtorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidtorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
