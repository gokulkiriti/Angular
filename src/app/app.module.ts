import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CounterComponent } from './counter/counter.component';
import { TodosComponent } from './todos/todos.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoFormComponent } from './todo-form/todo-form.component';
import { TodoComponent } from './todo/todo.component';
import { TodoService } from './todo.service';
import { SignInFormComponent } from './sign-in-form/sign-in-form.component';
import { SignUpFormComponent } from './sign-up-form/sign-up-form.component';
import { ValidtorComponent } from './validtor/validtor.component';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

export const ROUTES: Routes = [
  { path: 'counter', component: CounterComponent },
  { path: 'todos', component: TodosComponent },
  { path: 'signInform', component: SignInFormComponent },
  { path: 'signupform', component: SignUpFormComponent },
  { path: 'Validtor', component: ValidtorComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    CounterComponent,
    TodosComponent,
    TodoListComponent,
    TodoFormComponent,
    TodoComponent,
    SignInFormComponent,
    SignUpFormComponent,
    ValidtorComponent
  ],
  imports: [
    BrowserModule, FormsModule,  RouterModule.forRoot(ROUTES)
  ],
  providers: [TodoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
